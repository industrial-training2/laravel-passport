<?php

use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('/login', [UserController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user-details', [UserController::class, 'userDetails']);
});

